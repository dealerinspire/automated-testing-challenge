# Dealer Inspire Automated Testing Code Challenge
Welcome to the Dealer Inspire Automated Testing Code challenge.

## Getting Started
You'll need to install Selenium, ChromeDriver, jUnit, and JAVA.

## The Challenge
1.  Write a test to attempt a login with invalid credentials at https://conversations.dealerinspire.com
2.  Verify warning message displays as expected
3.  Write a test that attempts to reset the password with an invalid username
4.  Verify warning message displays as expected and verify text color

## Extra Credit
Write a Gherkin test scenario that describes the above features

## Submission Instructions
Submit a github public project for your code and include a video screenshare showing the automated test running.